#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/ioctl.h>
#include<net/if.h>
#include<errno.h>
#include<arpa/inet.h>
#include<unistd.h>
#include "getip.c"
#include <sys/wait.h>
#include <sys/types.h>
#include<signal.h>
#define PAUSE printf("Press Enter key to continue..."); fgetc(stdin);
#define filename "myifconfig"
#define gradetxt "grade.txt"

char* rebuild(char *ptr){
	int n = 0;
	char *p = ptr;
	while(*(p++) != '\0') n++;
	n+=1;
	char *buffer = malloc(n);
	p = ptr;
	char *s = buffer;
	while(*p != '\0'){
		if(*p == '\n' || *p == ' '){
			p++;
			continue;
		}
		*s = *p;
		s++;
		p++;
	}
	*s = '\0';
	return buffer;
}

int main(int argc, char* argv[]){
	int show = 15;
	int set = 15;

	if(argc != 3){
		printf("%s <path><studentID>\n",argv[0]);
		return -1;
	}

	char cwd[1000];
        getcwd(cwd, sizeof(cwd));
	if(chdir(argv[1]) < 0){
		perror("chdir");
		return 0;
	}
	
	FILE* fp = fopen(filename, "r");
	if (fp) {
		printf("file exist\n");
		// file exists
		fclose(fp);
	} else {
		printf("file don't exist\n");
		chdir(cwd);
		fp = fopen(gradetxt,"a");
		fprintf(fp,"%s nofile",argv[2]);
		fclose(fp);
		return 0;
	}	

	set_ip("127.0.0.2");
	printf("Test: Ifconfig\n\n");

	char* correctIP = get_ip();
	/* Test 1 */
	int pipefd[2];
	if(pipe(pipefd) < 0){
		perror("pipe");
		return -1;
	}	
	switch(fork()){
		case -1:
			perror("fork");
			return -1;
		case 0:
			dup2(pipefd[1],STDOUT_FILENO);
			setenv("PATH","",1);
			char *arg[4];
			arg[0] = filename;
                        arg[1] = "lo";
                        arg[2] = NULL;	
//                        arg[3] = NULL;
			if(execvp(arg[0],arg) < 0){
				perror("exec");
			}
			return 0;
	}
	wait(NULL);
	char buffer[50];
	
	fd_set master;
	struct timeval timeout;
	
	FD_ZERO(&master);
	FD_SET(pipefd[0],&master);	

	timeout.tv_sec = 5;
	timeout.tv_usec = 0;

	int ret = select(FD_SETSIZE, &master, NULL, NULL,&timeout);

	if(ret == 0){
		buffer[0] = '\0';
	}else if(ret < 0){
		perror("select");
		return 0;
	}else{
//		printf("ret = %d\n",ret);
		int n = read(pipefd[0],buffer,(sizeof buffer)-1);
		buffer[n] = '\0';
	}
	
	printf("----------\nTest 1: Show ip\n\n");	
	printf("Your result(./%s lo):\n%s\n",argv[1],buffer);	
	printf("Correct results:\n%s\n",correctIP);
	if(strcmp(rebuild(buffer),rebuild(correctIP))){
		show = 0;
	}
	char* test1 = strcmp(rebuild(buffer),rebuild(correctIP))?"FAIL":"PASS";	
	
	printf("Show IP: %s\n----------\n",test1);

	/* Test 2 */
	switch(fork()){
		case -1:
			perror("fork");
			return -1;
		case 0:
			setenv("PATH","",1);
			char *arg[4];
			arg[0] = filename;
                        arg[1] = "lo";
                        arg[2] = "127.0.0.5";
			arg[3] = NULL;
			if(execvp(arg[0],arg) < 0){
				perror("exec");
			}
			return 0;
	}
	wait(NULL);
	
	char *result = rebuild(get_ip());
	char *correct2 = "lo->inetaddr:127.0.0.5";
	if(strcmp(result,correct2)){
		set = 0;
	}
	char *test2 = strcmp(result,correct2)? "FAIL" : "PASS";
	printf("Test 2: Set ip\n\n");
	printf("Set lo 127.0.0.5\n");
	printf("%s\n",get_ip());
	printf("set IP : %s\n----------\n",test2);
	printf("Show IP: %d\nSet IP: %d\n",show,set);

	
	chdir(cwd);
	fp = fopen("grade.txt","a");
	fprintf(fp,"%s",argv[2]);
        fprintf(fp," %d ",show);
        fprintf(fp,"%d\n",set);
	fclose(fp);
	
}


