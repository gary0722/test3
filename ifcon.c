#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/ioctl.h>
#include<net/if.h>
#include<errno.h>
#include<arpa/inet.h>

#define SZ 1024

void get_ip(){
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    
    struct ifconf ifc;
    char* buff = malloc(SZ*sizeof(char));
    ifc.ifc_len = SZ;
    ifc.ifc_ifcu.ifcu_buf = buff;

    if(ioctl(sockfd, SIOCGIFCONF, &ifc)<0){
        perror("get_ip() error");
	//printf("ERROR at ioctl(), errno = %d\n", errno);
	exit(1);
    }
    
    char* i = buff;
    for(;i<buff+ifc.ifc_len;){
        struct ifreq* ifr = (struct ifreq*) i;
	struct sockaddr_in* sockaddr = (struct sockaddr_in*)&(ifr->ifr_ifru);
	if(strcmp(ifr->ifr_name, "lo")==0){
	    char IP[20];
	    inet_ntop(AF_INET, &(sockaddr->sin_addr), IP, 20);
	    printf("lo -> inet addr: %s\n", IP);
	    break;
	}
	i += sizeof(struct ifreq);
    }
    return;
}

void set_ip(char** argv){
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    
    struct ifreq ifr;
    strcpy(ifr.ifr_name, "lo");
    struct sockaddr_in* sockaddr = (struct sockaddr_in*)&(ifr.ifr_addr);
    sockaddr->sin_family = AF_INET;
    inet_pton(AF_INET, argv[2], &(sockaddr->sin_addr));

    if(ioctl(sockfd, SIOCSIFADDR, &ifr)<0){
        perror("set_ip() ERROR");
	//printf("ERROR at ioctl(), errno = %d\n", errno);
	exit(1);
    }
    
    return;
}

int main(int argc, char** argv){
    int opt = 0;
    if(argc==2){
        opt = 1;
	get_ip();
    }else if(argc==3){
        opt = 2;
	set_ip(argv);
    }else{
        printf("Usage: %s lo [IP]\n", argv[0]);
    }

    return 0;
}

